-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 16 Novembre 2017 à 10:12
-- Version du serveur :  5.7.20-0ubuntu0.16.04.1
-- Version de PHP :  7.0.25-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `esthievo`
--

-- --------------------------------------------------------

--
-- Structure de la table `Images`
--

CREATE TABLE `Images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `disp_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Images`
--

INSERT INTO `Images` (`id`, `name`, `disp_count`) VALUES
(3, 'cheek_outer_volume_fat.png', 5),
(4, 'cheek_outer_volume_thin.png', 4),
(5, 'chin_height_high.png', 3),
(6, 'chin_height_low.png', 4),
(7, 'chin_width_narrow.png', 5),
(8, 'chin_width_wide.png', 4),
(9, 'control.png', 4),
(10, 'eyes_high.png', 2),
(11, 'eyes_low.png', 5),
(12, 'eyes_narrowspaced.png', 5),
(13, 'eyes_widespaced.png', 4),
(14, 'lowerlip_curved_shape_narrow.png', 3),
(15, 'lowerlip_curved_shape_wide.png', 4),
(16, 'mouth_move_vertically_high.png', 4),
(17, 'mouth_move_vertically_low.png', 5),
(18, 'mouth_scale_horiz_narrow.png', 3),
(19, 'mouth_scale_horiz_wide.png', 4),
(20, 'nose_scale_vert_long.png', 4),
(21, 'nose_scale_vert_short.png', 4),
(22, 'upperlip_height_short.png', 1),
(23, 'upperlip_height_tall.png', 3);

-- --------------------------------------------------------

--
-- Structure de la table `Results`
--

CREATE TABLE `Results` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `image1` int(11) NOT NULL,
  `image2` int(11) NOT NULL,
  `winner` int(11) NOT NULL,
  `user_ID` int(11) NOT NULL,
  `select_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `sex_F` int(11) DEFAULT NULL,
  `sex_M` int(11) DEFAULT NULL,
  `birth` int(11) NOT NULL DEFAULT '1910',
  `country` varchar(255) DEFAULT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `pref_fem` tinyint(1) DEFAULT NULL,
  `pref_mal` tinyint(1) DEFAULT NULL,
  `pref_no` tinyint(1) NOT NULL DEFAULT '0',
  `vision` tinyint(1) NOT NULL DEFAULT '0',
  `colorblind` tinyint(1) NOT NULL DEFAULT '0',
  `complet_test` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Images`
--
ALTER TABLE `Images`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Results`
--
ALTER TABLE `Results`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Images`
--
ALTER TABLE `Images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `Results`
--
ALTER TABLE `Results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
