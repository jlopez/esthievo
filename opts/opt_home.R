
output$langWeb <- renderUI({
  actionButton("langButton", lang$home$button_lang$fr, class = "btn-primary")
})

output$play <- renderUI({
  a(lang$home$play_button$en, class="btn btn-success", href="#shiny-tab-Questionnaire", "data-toggle" = "tab")
})

output$home_desc <- renderUI({
  p(lang$home$intro$en)
})

#' Event sur le bouton de changement de langue
#' @return void
observeEvent(input$langButton, {
  if(web_lang == "english") {
    web_lang <<- "french"
    output$langWeb <- renderUI({
      actionButton("langButton", lang$home$button_lang$en, class = "btn-primary")
    })
    output$home_desc <- renderUI({
      p(lang$home$intro$fr)
    })
    output$play <- renderUI({
      a(lang$home$play_button$fr, class="btn btn-success", href="#shiny-tab-Questionnaire", "data-toggle" = "tab")
    })
  } else {
    web_lang <<- "english"
    output$langWeb <- renderUI({
      actionButton("langButton", lang$home$button_lang$fr, class = "btn-primary")
    })
    output$home_desc <- renderUI({
      p(lang$home$intro$en)
    })
    output$play <- renderUI({
      a(lang$home$play_button$en, class="btn btn-success", href="#shiny-tab-Questionnaire", "data-toggle" = "tab")
    })
  }
  
})

